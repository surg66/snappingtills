# Desctiption
Snapping tills - Don't Starve Together modification.

Aligns plowing to the grid.

LeftShift + RightMouseClick: Launch auto tilling on tile.
Key "L": Toggle snap modes, off/optimized/4x4/3x3/2x2/hexagon.
Optimized mode checks for an adjacent soil tile, if not found adjacent soil tile then uses 4x4 else uses 3x3.
You can bind key in configure mod.

Important:
    "Geometric Placement" has priority, so that to use this mod,
    "Geometric Placement" must be OFF in game (key B -> "off")
    or disabled "snap till" in options (key B -> disable "snap till")
    (look at screenshots)

Сontroller support.
Compatible with Geometric Placement
Compatible with ActionQueue Reborn (ActionQueue RB2)

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2302837868)
